"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const WsHelper_1 = require("./WsHelper");
const APIHelper_1 = require("./APIHelper");
class ConnectionManager {
    constructor(ConnectionOptions) {
        this.UseWebSocket = true;
        this.UseAPI = true;
    }
    configureAPI(baseUrl, options) {
        console.log("Configuring API for: " + baseUrl);
        this.api = new APIHelper_1.APIHelper(baseUrl);
        return this.api;
    }
    connectToWebSocket(url, options) {
        console.log("Connecting to WebSocket At: " + url);
        let wsConnection = new WsHelper_1.WsHelper(url, options);
        this.ws = wsConnection;
        return wsConnection;
    }
    /**
     * sendMessage
     */
    sendMessage(data) {
        this.ws.send(data);
    }
}
exports.ConnectionManager = ConnectionManager;
//# sourceMappingURL=ConnectionManager.js.map