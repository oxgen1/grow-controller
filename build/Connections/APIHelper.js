"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const node_fetch_1 = require("node-fetch");
class APIHelper {
    constructor(baseUrl) {
        if (this.doesEndWithBSlash(baseUrl)) {
            APIHelper.baseUrl = baseUrl;
        }
        else {
            APIHelper.baseUrl = baseUrl + '/';
        }
    }
    /**
     * Call node-fetch.Fetch with POST paraemters and JSON
     * @param namespace
     * @param endpoint
     * @param callback
     */
    post(namespace, endpoint, data, callback) {
        return __awaiter(this, void 0, void 0, function* () {
            let URL = `${APIHelper.baseUrl}${namespace}/${endpoint}`;
            if (typeof data === 'string') {
                // Do nothing
            }
            else if (typeof data === 'object') {
                data = JSON.stringify(data);
            }
            let options = {
                method: 'post',
                body: data,
                headers: { 'Content-Type': 'application/json' },
            };
            console.log("URL::::::::::::::::::::", URL);
            node_fetch_1.default(URL, options)
                .then(res => res.json()
                .then(json => callback(json)));
        });
    }
    doesEndWithBSlash(string) {
        return (string.indexOf('/', string.length - 1) === string.length - 1);
    }
}
exports.APIHelper = APIHelper;
//# sourceMappingURL=APIHelper.js.map