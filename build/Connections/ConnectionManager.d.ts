import { WsHelper } from './WsHelper';
import { APIHelper } from './APIHelper';
interface ConnectionManagerOptions {
}
export declare class ConnectionManager {
    UseWebSocket: boolean;
    UseAPI: boolean;
    ws: WsHelper;
    api: APIHelper;
    constructor(ConnectionOptions: ConnectionManagerOptions);
    configureAPI(baseUrl: string, options: any): APIHelper;
    connectToWebSocket(url: string, options: any): WsHelper;
    /**
     * sendMessage
     */
    sendMessage(data: any): void;
}
export {};
//# sourceMappingURL=ConnectionManager.d.ts.map