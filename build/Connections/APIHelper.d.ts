export declare class APIHelper {
    static baseUrl: string;
    constructor(baseUrl: string);
    /**
     * Call node-fetch.Fetch with POST paraemters and JSON
     * @param namespace
     * @param endpoint
     * @param callback
     */
    post(namespace: string, endpoint: string, data: string | object, callback: Function): Promise<void>;
    private doesEndWithBSlash;
}
//# sourceMappingURL=APIHelper.d.ts.map