import * as ws from 'ws';
import { ClientOptions } from 'ws';
export declare class WsHelper {
    static sendOptions: {};
    ws: ws;
    constructor(url: string, options: ClientOptions);
    send(data: any): void;
}
//# sourceMappingURL=WsHelper.d.ts.map