"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const EventEmitter = require('events');
const __1 = require("../");
//const cron = require('node-cron');
/**
 *
 * @memberof Devices
 * @class Controller
 * @extends {Device}
 */
class Controller extends __1.Device {
    constructor(config) {
        super(config);
        //this.action = config.action; 
        //this.type = config.action.constructor.name;
        this.actions = [];
        this.loaded = false;
    }
    /**
     * @param {String} event
     * @param {Function} action
     * @memberof Controller
     */
    registerActionEvent(event, action) {
        let data = {
            eventId: this.id + '.' + event,
            action: action
        };
        return this.actions.push(data);
    }
    /** Gets the Status of this Controller.
     * @method
     * @returns {SensorStatus}
     */
    getStatus() {
        return {
            name: this.name,
            enabled: this.enabled,
            type: this.constructor.name
        };
    }
    _checkAction() {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
    enable() {
        if (!this.loaded) {
            this.init();
        }
        this.enabled = true;
    }
    disable() {
        throw new Error("Method not implemented.");
    }
    init() {
        this._validate();
        this.actions.forEach(event => {
            this.gardenEmitter.addListener(event.eventId, event.action);
        });
        /* Old Action Code:
        if(this.action instanceof ControllerTimer){
             try {
                 let action = this.action;
                 let cronexp = action.cronexp;
                 let actionObj = cron.schedule(cronexp, action.actionFunc);
                 action.cron = actionObj;
                 console.log('Cron Schedular Started: ', action.cron);
             } catch(err) {
                 throw new Error(err);
             }
         } */
        this.loaded = true;
    }
    _validate() {
    }
}
exports.Controller = Controller;
class ControllerAction {
    constructor(config) {
        this.name = config.name;
    }
}
/*
class ControllerTimer extends ControllerAction{
    constructor(config) {
        super(config);
        if(!cron.validate(config.cronexp)) throw new Error('Invalid Cron Expression in Controller Definition');
        this.cronexp = config.cronexp;
        this.actionFunc = config.actionFunc;
    }

    get cron(){
        if(typeof cron !== 'undefined'){
            return this.cronObj;
        } else {
            throw new Error('You Cannot Get A cron Object before it is defined');
        }
    }

    set cron(cron){
        this.cronObj = cron;
    }

    getStatus(){
        return this.cronObj;
    }
} */
//module.exports.ControllerTimer = ControllerTimer;
//# sourceMappingURL=Controller.js.map