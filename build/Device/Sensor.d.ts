/**
 * @file - Provides the Sensor Object
 */
import { Device } from "../";
/**
 * @memberof Devices
 * @class Sensor
 * @extends Device
 * @constructor
 * @param {sensorData} sensorData - An Settings for the Sensor Object
 * @param {Object} sensorData.dataFields - This Uses Sequlize Data Types to Build the Table so this must be accurate, @example {FieldName1: {type: Sequelize.FLOAT}, FieldName2: {type: Sequelize.FLOAT}}
 * @param {int} sensorData.sampleRate - Sample Rate in ms
 * @example
 * var test = new Sensor({
    id: 1,
    name: 'test',
    dataFields: {Temperature: {type: Sequelize.FLOAT}, Humidity: {type: Sequelize.FLOAT}},
    sampleRate: 2000,
    verbose: true
 * });
 *
 */
interface SensorData {
    id: string;
    name: string;
    dataFields: object;
    sampleRate: number;
    verbose?: boolean;
    deviceType: string;
}
export declare class Sensor extends Device {
    dataFields: Table;
    sampleRate: number;
    dataStream: ReadableStream;
    constructor(sensorData: SensorData);
    /**
     * Reads Sensor Data
     *
     * @returns {Object}
     */
    read(): any;
    /** Gets the Status of this Sensor.
     * @method
     * @returns {SensorStatus}
     */
    getStatus(): {
        name: string;
        enabled: boolean;
        type: string;
    };
    /** Enable or Start the Sensor, Will stop Polling for data
     * @method
     */
    enable(): this;
    /** Disable to Stop the Sensor, Will stop Polling for data
     * @method
     */
    disable(): void;
    intervalTimer(intervalTimer: any): void;
    /**
     * Gets the Most Recent Number of Fields from the database
     * @method
     * @param {int} numOfPoints
     *
     * @returns {Array} LatestData
     */
    getLatestData(numOfPoints: any): void;
    /**
     * Gets Sensor Data inbetween 2 time periods, with an optional sorting parameter.
     *
     * @param {*} startTime
     * @param {*} endTime
     * @param {*} sorting
     */
    getData(startTime: any, endTime: any, sorting?: string): void;
    /**
     * @async
     * @abstract
     */
    sample(): Promise<void>;
    /**
     * @private
     * @async
     * @method
     */
    _logData(): Promise<void>;
    /**
     * Initialize Interval Timer, Should not be called outside of this class.
     * @private
     * @method
     */
    _init(): void;
    /**
     * @private
     * @param {*} dataObject
     */
    _validateData(dataObject: any): boolean;
    /**
     * @private
     * @method
     * @returns {Object} - Database Status Object
     */
    _getDatabaseStatus(): void;
}
export {};
//# sourceMappingURL=Sensor.d.ts.map