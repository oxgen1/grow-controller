/**
 * @file
 * @namespace Devices
 */
import { GardenEvent } from '../';
/** An Abstarct Base Class to Represent Devices
 * @memberof Devices
 * @class Device
 * @abstract
 * @constructor
 * @param {Object} obj - Device Settings Object
 * @param {string} obj.id - Device unique name
 * @param {string} obj.deviceType - Device Type
 * @param {boolean} obj.verbose - Enable/ Disabnle Verbose Debugging
*/
export declare abstract class Device {
    id: string;
    name: string;
    deviceType: string;
    enabled: boolean;
    verbose: boolean;
    logOut: ReadableStream;
    gardenEmitter: GardenEvent;
    constructor(obj: {
        name: string;
        deviceType: string;
        verbose?: boolean;
    });
    /**
     * Sets the Event Emitter for this Device. (Will Come from the Garden Object)
     * @type {Garden.GardenEvent}
     */
    set emitter(emitter: GardenEvent);
    /**
     * Gets the Event Emitter for this Device. (Will Come from the Garden Object)
     */
    get emitter(): GardenEvent;
    /** Enable Device
     * @abstract
     * @method
     */
    abstract enable(): void;
    /** Disable Device
     * @abstract
     * @method
     */
    abstract disable(): void;
    /**
     * @abstract
     * @method
     */
    abstract getStatus(): void;
}
//# sourceMappingURL=Device.d.ts.map