import { Device } from '../';
/**
 *
 * @memberof Devices
 * @class Controller
 * @extends {Device}
 */
export declare class Controller extends Device {
    actions: any[];
    loaded: boolean;
    constructor(config: any);
    /**
     * @param {String} event
     * @param {Function} action
     * @memberof Controller
     */
    registerActionEvent(event: String, action: Function): number;
    /** Gets the Status of this Controller.
     * @method
     * @returns {SensorStatus}
     */
    getStatus(): {
        name: string;
        enabled: boolean;
        type: string;
    };
    _checkAction(): Promise<void>;
    enable(): void;
    disable(): void;
    init(): void;
    _validate(): void;
}
//# sourceMappingURL=Controller.d.ts.map