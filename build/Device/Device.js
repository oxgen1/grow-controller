"use strict";
/**
 * @file
 * @namespace Devices
 */
Object.defineProperty(exports, "__esModule", { value: true });
const { Readable } = require('stream');
//const EventEmitter = require('events');
/** An Abstarct Base Class to Represent Devices
 * @memberof Devices
 * @class Device
 * @abstract
 * @constructor
 * @param {Object} obj - Device Settings Object
 * @param {string} obj.id - Device unique name
 * @param {string} obj.deviceType - Device Type
 * @param {boolean} obj.verbose - Enable/ Disabnle Verbose Debugging
*/
class Device {
    constructor(obj) {
        this.id = obj.name.replace(" ", "-").toLowerCase();
        this.name = obj.name;
        this.deviceType = obj.deviceType;
        /**
         * @name Device#enabled
         * @type {boolean}
         * @default false
        */
        this.enabled = false;
        /**
         * @name Device#verbose
         * @type {boolean}
         * @default false
         */
        this.verbose = (obj.verbose !== undefined) ? obj.verbose : false;
        this.logOut = new Readable({
            objectMode: false,
            read() { }
        });
    }
    /**
     * Sets the Event Emitter for this Device. (Will Come from the Garden Object)
     * @type {Garden.GardenEvent}
     */
    set emitter(emitter) {
        this.gardenEmitter = emitter;
    }
    /**
     * Gets the Event Emitter for this Device. (Will Come from the Garden Object)
     */
    get emitter() {
        return this.gardenEmitter;
    }
}
exports.Device = Device;
//# sourceMappingURL=Device.js.map