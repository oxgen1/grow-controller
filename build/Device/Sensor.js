"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @file - Provides the Sensor Object
 */
const __1 = require("../");
const stream_1 = require("stream");
class Sensor extends __1.Device {
    constructor(sensorData) {
        super(sensorData);
        this.dataFields = sensorData.dataFields;
        this.dataFields.Time = { type: 'INTEGER', allowNull: false };
        /**
         * @name Sensor#model
         * @type {int}
         */
        this.sampleRate = sensorData.sampleRate;
        //this.emitter = emitter;
        /**Defines a Sequlize Database Model
         * @name Sensor#model
         * @type {SequlizeModel}
        */
        //this.model = sql.define(this.name +"-" +this.id, this.dataFields);
        /**Define a Readable Stream for Data
         * @type {ReadableStream}
         */
        this.dataStream = new stream_1.Readable({
            objectMode: true,
            read() { }
        });
    }
    /**
     * Reads Sensor Data
     *
     * @returns {Object}
     */
    read() {
        if (!this.enabled) {
            return false;
        }
        return this.dataStream.read();
    }
    /** Gets the Status of this Sensor.
     * @method
     * @returns {SensorStatus}
     */
    getStatus() {
        return {
            name: this.name,
            enabled: this.enabled,
            type: this.constructor.name
        };
    }
    export() {
        return Object.assign(this.getStatus(), { data: this.read() }, { timestamp: Date.now() });
    }
    /** Enable or Start the Sensor, Will stop Polling for data
     * @method
     */
    enable() {
        if (!this.enabled) {
            try {
                this._init();
                this.gardenEmitter.emit('sensorEnabled', (this));
                this.enabled = true;
            }
            catch (err) {
                throw new Error(err);
            }
            finally {
            }
        }
        else {
            return this;
        }
    }
    /** Disable to Stop the Sensor, Will stop Polling for data
     * @method
     */
    disable() {
        if (this.enabled) {
            try {
                clearInterval(this.intervalTimer);
                this.gardenEmitter.emit('sensorDisabled', (this));
            }
            catch (err) {
            }
            finally {
                this.enabled = false;
            }
        }
    }
    /**
     * Gets the Most Recent Number of Fields from the database
     * @method
     * @param {int} numOfPoints
     *
     * @returns {Array} LatestData
     */
    getLatestData(numOfPoints) {
        //this.model.findAll({limit: numOfPoints, order: ['Time', 'DESC']}).then((result) => {
        //    if (this.verbose) log.debug("getLatestData Called: ", result)('sensor');
        //});
    }
    /**
     * Gets Sensor Data inbetween 2 time periods, with an optional sorting parameter.
     *
     * @param {*} startTime
     * @param {*} endTime
     * @param {*} sorting
     */
    getData(startTime, endTime, sorting = 'DESC') {
    }
    /**
     * @async
     * @abstract
     */
    sample() {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
    /**
     * @private
     * @async
     * @method
     */
    _logData() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let data = yield this.sample();
                //Are all the Field names and datatypes okay?
                if (!this._validateData(data)) {
                    console.error('Invalid Data/DataFields');
                }
                /**
                 * @event sensor.data
                 */
                this.gardenEmitter.emit('sensor.data', this.id, data);
                this.dataStream.push(data);
            }
            catch (err) {
                throw new Error(err.message);
            }
        });
    }
    /**
     * Initialize Interval Timer, Should not be called outside of this class.
     * @private
     * @method
     */
    _init() {
        //this.model.sync();
        var _this = this;
        this.intervalTimer = setInterval(function () {
            _this._logData();
        }, this.sampleRate);
    }
    /**
     * @private
     * @param {*} dataObject
     */
    _validateData(dataObject) {
        let tableNames = Object.keys(this.dataFields);
        let dataNames = Object.keys(dataObject);
        //Preform a check in ensure that the data fields the sensor is returning and return the values it is expecting.
        var diff = dataNames.filter(function (i) {
            return tableNames.indexOf(i) < 0;
        });
        //If we find any Throw an Error, for now we will not handle this but in the future we might not to cause a crash just because one sensor is out of alightment.
        if (diff.length > 0) {
            throw new Error('You are returning an incorrect Data Field the expected Field Name should be: ' + diff);
        }
        return true;
    }
    /**
     * @private
     * @method
     * @returns {Object} - Database Status Object
     */
    _getDatabaseStatus() {
    }
}
exports.Sensor = Sensor;
/*class SensorEvent extends EventEmitter {
    constructor(){
        super();
    }
} */
//# sourceMappingURL=Sensor.js.map