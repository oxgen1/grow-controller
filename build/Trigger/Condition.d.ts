import { Sensor } from './../';
/**
 * @memberof Conditions
 * @abstract
 */
declare class Condition {
    _isEvaluable: boolean;
    _isTriggerAble: boolean;
    _lockOut: boolean;
    _event: string;
    _lastTriggered: any;
    constructor();
    /**
     * Gets the A computed Id based on the Type, and other props
     * @readonly
     */
    get id(): string;
    /** Gets If the Condition is Evaluable */
    get isEvaluable(): boolean;
    /** Sets If the Condition is Evaluable
     * @type {boolean}
     */
    set isEvaluable(boolean: boolean);
    /**Gets if the Condition is Triggerable  (i.e if this condition can by itself trigger an action*/
    get isTriggerAble(): boolean;
    /**Sets if the Condition is Triggerable  (i.e if this condition can by itself trigger an action
     * @type {boolean}
    */
    set isTriggerAble(boolean: boolean);
    /**Gets if the Condition is a lock out condition (I.e If this condition is not met no actions can be emitted) */
    get lockOut(): boolean;
    /**Sets if the Condition is a lock out condition (I.e If this condition is not met no actions can be emitted)
     * @type {boolean}
    */
    set lockOut(boolean: boolean);
    /**Gets the Interval Trigger Level Event Name */
    get event(): string;
    /**Gets the Interval Trigger Level Event Name
     * @type {string}
    */
    set event(string: string);
    /**Gets the Last Triggered Timestamp */
    get lastTriggered(): any;
    /**Sets the Last Triggered Timestamp
     * @type {timestamp}
    */
    set lastTriggered(timestamp: any);
    /**Gets The Type (Constructor.name) */
    getType(): string;
}
/**
 *
 * @class
 * @extends {Conditions.Condition}
 * @memberof Conditions
 * @abstract
 */
declare class ThresholdCondition extends Condition {
    operator: any;
    threshold: any;
    ref: string;
    sensor: Sensor;
    dataField: string;
    value: any;
    function: (value: any) => boolean;
    humanReadable(): string;
}
/**
 * @class
 * @extends {Conditions.Condition}
 * @memberof Conditions
 * @abstract
 */
declare class IntervalCondition extends Condition {
    _interval: number;
    ref: string;
    constructor();
    get interval(): number;
    set interval(seconds: number);
    humanReadable(): string;
}
declare const _default: {
    createCondition: (threshold: any, operator: any, value: any, field?: string, lockOut?: boolean) => ThresholdCondition;
    createInterval: (interval: any, lockOut?: boolean) => IntervalCondition;
};
export = _default;
//# sourceMappingURL=Condition.d.ts.map