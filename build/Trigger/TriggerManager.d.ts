import { Garden, GardenEvent } from "..";
/**
 * The Trigger Manager Class
 */
export declare class TriggerManager {
    gardenEmitter: GardenEvent;
    emitter: any;
    triggers: any[];
    debug: boolean;
    /**
     * Creates an instance of TriggerManager.
     * @constructs TriggerManager
     * @param {Garden} garden {@link Garden}
     * @memberof Triggers
     */
    constructor(garden: Garden);
    /**
     * Creates A Trigger Object
     * @constructs Trigger
     * @param {Condition} condition {@link Condition}
     * @param {String} actionEvent - Event Listener Name connected with the Controller Action Will. Emit GardenEvent Emitter
     * @returns {Triggers#Trigger}
     * @memberof Triggers
     */
    createTrigger(condition: any, actionEvent: any): {
        id: number;
        emitter: any;
        actionEvent: any;
        internalEvent: string;
        logic: any[];
        triggerLimiter: any;
        getInitCondition: () => any;
        getLogic: () => any;
        and: (condition: any) => any;
        or: (condition: any) => any;
        checkOtherConditions(condition: any): any;
        evaluateOtherConditions: (condition: any) => any;
        init: () => any;
    };
}
//# sourceMappingURL=TriggerManager.d.ts.map