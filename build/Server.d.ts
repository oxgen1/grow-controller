/// <reference types="node" />
import { Garden, ConnectionManager } from './';
import { EventEmitter } from 'events';
interface ServerAuth {
    gardenPublicKey?: string;
    gardenPrivateKey?: string;
}
interface ServerOptions {
    auth?: ServerAuth;
    enableIPC?: boolean;
    websocketIP?: string;
    apiIP?: string;
}
/**
 * This is the Start Point where the API is intilizied.
 *
 * Steps as I remeber (Check as I go)
 * 1. Parse Server Options
 * 2. Enable Connection Managers
 * 3. Enable Garden
 *
 */
export declare class Server extends EventEmitter {
    garden: Garden;
    connectionManager: ConnectionManager;
    private options;
    constructor(garden: Garden, options?: ServerOptions);
    connect(gardenId: String): void;
    start(): void;
    private _registerIPCEvents;
    private formatter;
}
export {};
//# sourceMappingURL=Server.d.ts.map