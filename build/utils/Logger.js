"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const util = require("util");
const fs = require("fs");
const path = require("path");
if (!String.prototype.findWithWildcards) {
    // eslint-disable-next-line no-extend-native
    String.prototype.findWithWildcards = function (needle) {
        var escapeRegex = (str) => str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
        return new RegExp("^" + needle.split("*").map(escapeRegex).join(".*") + "$").test(this);
    };
}
class Logger {
    constructor({ levels = {
        error: 0,
        warn: 1,
        info: 2,
        debug: 3
    }, files = {
        all: null,
        error: '/logs/error.log',
        warn: '/logs/warn.log',
        info: '/logs/info.log',
        debug: '/logs/debug.log'
    }, stoutOpts = {
        noDebug: ['error', 'warn']
    } } = {}) {
        this.levels = levels;
        this.files = {};
        this.stoutOpts = stoutOpts;
        this.stdout = process.stdout;
        this.stderr = process.stderr;
        var ENV = process.env;
        var targetLevels, scopes;
        if (ENV.hasOwnProperty('DEBUG') && ENV.DEBUG) {
            targetLevels = stoutOpts.hasOwnProperty('debug') ? this.stoutOpts.debug.map(e => [e, true]) : [
                ['all', true]
            ];
            if (ENV.hasOwnProperty('DEBUG_SCOPES')) {
                scopes = ENV.DEBUG_SCOPES.toUpperCase();
                this.scopes = scopes.split(',');
                console.log(this.scopes);
            }
            else {
                this.scopes = ['*'];
            }
        }
        else {
            targetLevels = this.stoutOpts.noDebug.map(e => [e, true]);
        }
        this.stouts = Object.fromEntries(targetLevels);
        try {
            for (let file in files) {
                if (typeof files[file] === 'string') {
                    let filepath = path.join(__gardendir, files[file]);
                    console.log(filepath);
                    try {
                        this.files[file] = fs.createWriteStream(filepath, {
                            flags: "a"
                        });
                    }
                    catch (err) {
                        console.error("Error Creating Log Files Streams:\n", err);
                    }
                }
            }
        }
        catch (err) {
            console.error("Error Creating Log Files Streams:\n", err);
        }
        for (let level in levels) {
            this[level] = function (message, ...rest) {
                let formatScope = (scope = '*') => {
                    if (scope == '*') {
                        return "[All]";
                    }
                    else {
                        return `[${scope.toUpperCase()}]`;
                    }
                };
                var log = (scope = '*', inspectOpts = {
                    colors: true
                }) => {
                    rest.unshift(message);
                    rest.unshift(inspectOpts);
                    let inspects = util.formatWithOptions.apply(null, rest);
                    let out = `${formatScope(scope)}: ${inspects}`;
                    if (this.files.hasOwnProperty('all'))
                        this.files.all.write(out + "\n");
                    if (this.files.hasOwnProperty(level))
                        this.files[level].write(out + "\n");
                    if (scope !== '*') {
                        if (this.scopes.findIndex(e => scope.toUpperCase().findWithWildcards(e)) === -1) {
                            return;
                        }
                    }
                    if (this.stouts.hasOwnProperty('all')) {
                        if (level === 'error')
                            this.stderr.write(out + "\n");
                        else
                            this.stdout.write(out + "\n");
                    }
                    if (this.stouts.hasOwnProperty(level)) {
                        if (level === 'error')
                            this.stderr.write(out + "\n");
                        else
                            this.stdout.write(out + "\n");
                    }
                };
                return (scope = '*') => log(scope);
            };
        }
        ;
    }
}
exports.Logger = Logger;
//# sourceMappingURL=Logger.js.map