/// <reference types="node" />
/**
 * Creates Log Functions based on params, Levels create functions for each level, and write to different files, and streams
 * Env Params Accepted: GARDEN_ROOT_DIR = Project Path | DEBUG_SCOPES * is wildcard | DEBUG
 * @namespace Utils
 * @memberof Utils
 * @name Logger
 * @constructor
 * @param {Object} [options] - The options object
 * @param {Object} [options.levels = {error: 0, warn: 1, info: 2, debug: 3}] - The Different Levels eg. error, warn, info, debug
 * @param {Object} [options.files = { all: null, error: '/logs/error.log', warn: '/logs/warn.log', info: '/logs/info.log', debug: '/logs/debug.log' }] - The Debugging Files, options are based on levels with the added all null can be defined to disable a specific log file
 * @param {Object} [options.stoutOpts = { noDebug: ['error', 'warn'] }] - Options for the stdout control.
 */
interface LoggerOptions {
    levels?: object;
    files?: object;
    stoutOpts?: object;
}
export declare class Logger {
    levels: object;
    files: object;
    stoutOpts: object;
    stdout: NodeJS.WriteStream;
    stderr: NodeJS.WriteStream;
    scopes: any;
    stouts: any;
    constructor({ levels, files, stoutOpts }?: LoggerOptions);
}
export {};
//# sourceMappingURL=Logger.d.ts.map