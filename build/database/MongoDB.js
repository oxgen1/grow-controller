"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongodb_1 = require("mongodb");
const mongoUser = encodeURIComponent('jack');
const mongoPw = encodeURIComponent('SuperServer9!');
const mongoDbURL = `mongodb://${mongoUser}:${mongoPw}@localhost:27017`;
const MongoDBName = 'Gardens';
const mongoDB = new mongodb_1.MongoClient(mongoDbURL);
var _db;
class MongoDb {
    connect() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield mongoDB.connect();
                _db = mongoDB.db(MongoDBName);
            }
            catch (err) {
                console.error('There was an error connecting to MongoDB: ', err);
            }
            return _db;
        });
    }
    getDB() {
        return __awaiter(this, void 0, void 0, function* () {
            return _db;
        });
    }
}
var mongoDb = new MongoDb();
exports.mongoDb = mongoDb;
//# sourceMappingURL=MongoDB.js.map