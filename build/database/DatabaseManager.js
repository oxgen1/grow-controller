"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//TODO Handle Indexes
//TODO Remove Spaces
//TODO Warning Messages
//TODO Support ENUMS
//TODO AUTO INCREMENT must be KEY
class TableMaster {
    constructor(table) {
        console.log(table);
        table.fields.forEach(c => {
            return this.checkColumn(c);
        });
        this.checkColumn(table.fields[0]);
        table = this.checkTableOpts(table);
        console.log(table);
        this.SQL = `CREATE TABLE`;
        if (table.options.IfNotExists)
            this.SQL += ` IF NOT EXISTS`;
        this.SQL += ` ${table.tableName}`;
        if (typeof this.PKey !== 'undefined') {
            this.SQL += `(${this.createColumnStmnt(table)} PRIMARY KEY (${this.PKey}) )`;
        }
        else {
            this.SQL += `(${this.createColumnStmnt(table)})`;
        }
        console.log(this.SQL);
    }
    createColumnStmnt(table) {
        var columns = table.fields;
        var columnStatments = '';
        var hasPrimaryKey = false;
        var i = 0;
        for (const c of columns) {
            let rowStmnt = `${c.name} ${c.type}(${c.typeLength})`;
            if (c.options.Unsigned) {
                rowStmnt += ` unsigned`;
            }
            if (c.options.NotNull) {
                rowStmnt += ` NOT NULL`;
            }
            if (typeof c.options.Default !== 'undefined') {
                rowStmnt += ` DEFAULT ${c.options.Default}`;
            }
            else {
                if (c.options.HasDefault)
                    rowStmnt += ` DEFAULT NULL`;
            }
            if (c.options.AutoIncrement) {
                rowStmnt += ` AUTO_INCREMENT`;
            }
            if (c.options.PrimaryKey) {
                if (!hasPrimaryKey) {
                    this.PKey = c.name;
                }
                else {
                    console.warn('You have More then one Primary Key and one was already set defaulting to First one set: ', this.PKey);
                }
            }
            if (hasPrimaryKey || i !== columns.length - 1) {
                rowStmnt += `, `;
            }
            columnStatments += rowStmnt;
            i++;
        }
        return columnStatments;
    }
    checkColumn(column) {
        if (typeof column.options === 'undefined') {
            column.options = {
                PrimaryKey: false,
                AutoIncrement: false,
                Unsigned: false,
                NotNull: false,
                HasDefault: false
            };
        }
        else {
            if (column.type === "tinyint(1)" /* BOOLEAN */) {
                column.options.PrimaryKey = false;
                column.options.AutoIncrement = false;
                column.options.Unsigned = true;
            }
            else if (column.type === "varchar" /* STRING */ || column.type === "char" /* CHAR */) {
                column.options.AutoIncrement = false;
                column.options.Unsigned = false;
                if (typeof column.options.Default !== 'undefined') {
                    column.options.Default = `'${column.options.Default}'`;
                }
            }
        }
        if (typeof column.options.NotNull === undefined) {
            column.options.NotNull = false;
        }
        if (typeof column.options.HasDefault === undefined) {
            column.options.HasDefault = false;
        }
        return column;
    }
    checkTableOpts(table) {
        if (typeof table.options === 'undefined') {
            table.options = {
                DateCreatedCol: true,
                IfNotExists: true,
                LastUpdatedCol: true,
                TimeBased: false
            };
        }
        else {
            if (typeof table.options.DateCreatedCol === undefined) {
                table.options.DateCreatedCol = true;
            }
            if (typeof table.options.IfNotExists === undefined) {
                table.options.IfNotExists = true;
            }
            if (typeof table.options.LastUpdatedCol === undefined) {
                table.options.LastUpdatedCol = true;
            }
            if (typeof table.options.TimeBased === undefined) {
                table.options.TimeBased = false;
            }
        }
        return table;
    }
}
var table = {
    tableName: 'test',
    fields: [{
            name: 'id',
            type: "int" /* INT */,
            typeLength: 11,
            options: {
                Unsigned: true,
                AutoIncrement: true,
                NotNull: true
            }
        },
        {
            name: 'Name',
            type: "varchar" /* STRING */,
            typeLength: 12,
            options: {
                Default: '12121'
            }
        }
    ]
};
var TableQry = new TableMaster(table);
console.log('test');
//# sourceMappingURL=DatabaseManager.js.map