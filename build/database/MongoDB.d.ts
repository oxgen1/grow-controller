import { Db } from 'mongodb';
declare class MongoDb {
    connect(): Promise<Db>;
    getDB(): Promise<Db>;
}
declare var mongoDb: MongoDb;
export { mongoDb };
//# sourceMappingURL=MongoDB.d.ts.map