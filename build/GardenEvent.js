"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const EventEmitter = require('events');
const _1 = require(".");
/**
 * @memberof Garden
 * @class GardenEvent
 * @extends {EventEmitter}
 */
class GardenEvent extends EventEmitter {
    constructor() {
        super();
        // THis can later moved in to a static function or something which can be called from server.connect(); 
        this.on('sensorEnable', (sensor) => {
            console.log('Sensor Enabled: ' + sensor.name);
        });
        this.on('sensorDisable', (sensor) => {
            console.log('Sensor Disabled: ' + sensor.name);
        });
        this.on('garden.enable', (garden) => {
            //this.getConnectionManager.connectToWebSocket("ws://localhost:7171/", {});
            console.log('Garden Enabled: ', garden.export());
            var data = JSON.stringify(garden.export());
            console.log(data);
            /*fetch("http://localhost:6969/garden/handshake", {
                method: 'post',
                body:    data,
                headers: { 'Content-Type': 'application/json' },
            }).then(res => res.json())
            .then(json => console.log(json)); */
        });
        this.on('sensor.data', (sensorId, data) => {
            console.log("getting Sensor Data: ", sensorId, data);
        });
        this.on('error', (err) => {
            console.log(err);
        });
    }
    get getConnectionManager() {
        if (this.hasOwnProperty('connectionManager') && this.connectionManager instanceof _1.ConnectionManager) {
            return this.connectionManager;
        }
        else {
            console.log('Called Connection Manager before it was set');
            return new Proxy(new _1.ConnectionManager(null), {
                apply: function (_target, _thisArg, _argumentsList) {
                    return true;
                },
                get: function (_target, _prop, _receiver) {
                    return () => { return false; };
                }
            });
        }
    }
    set setConnectionManager(connectionManager) {
        this.connectionManager = connectionManager;
    }
}
exports.GardenEvent = GardenEvent;
//# sourceMappingURL=GardenEvent.js.map