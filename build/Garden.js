"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _1 = require("./");
const dotenv = require("dotenv");
dotenv.config();
//const mongoDB = require('./').mongoDB;
/**
 *
 * @example
 * var gardenEvent = new GardenEvent(); // Defind Garden Event Emitter
 * var testGarden = new Garden('testGarden', 'Test Desc', gardenEvent); //Create Garden
 * var sensor1 = testGarden.loadSensor('SensorTest'); // Load A Sensor (FilePath Relitive to __gardendir/Sensors)
 * testGarden.loadSensor('TestSensor1');
 * testGarden.loadController('ControllerTest'); //Load A Contrller (FilePath Relitive to __gardendir/Controllers)
 */
class Garden {
    constructor(name, desc, emitter) {
        this.name = name;
        this.desc = desc;
        /**
         * @name Garden#sensors
         * @type {Sensor[]}
         */
        this.sensors = [];
        /**
         * @name Garden#controllers
         * @type {Controller[]}
         */
        this.controllers = [];
        this.devicesFileList = [];
        /**
         * @name Garden#emitter
         * @type {Garden.GardenEvent}
         */
        this.emitter = emitter;
        /**
         * @name Garden#enabled
         * @type {Boolean}
         * @default false
         */
        this.enabled = false;
        //mongoDB.createConnection(this.name).then(() => {});
    }
    /** Gets the Garden Status of all loaded Devices
     *
     * @return {GardenStatus} A Garden Status Object
     */
    getGardenStatus() {
        let gardenStatus = {
            garden: this.name,
            sensors: [],
            controllers: []
        };
        gardenStatus.sensors = [];
        this.sensors.forEach(sensor => {
            //console.log(sensor.getStatus());
            let sensorStatus = sensor.getStatus();
            gardenStatus.sensors.push(sensorStatus);
        });
        this.controllers.forEach(controller => {
            console.log(controller.getStatus());
            let controllerStatus = controller.getStatus();
            gardenStatus.controllers = [];
            gardenStatus.controllers.push(controllerStatus);
        });
        console.log("gardenStatus", gardenStatus);
        return gardenStatus;
    }
    /**
     * Loads and Initializes a Sensor Device
     * Does three things: 1. attaches to Garden.[id] and Garden.sensors[]|controllers[] 2. Adds Garden Emitter Property 3. Add Garden Logger Property
     * @param {string} filePath
     * @returns {Sensor|Controller} - the Loaded Sensor Object
     */
    attach(filePath) {
        try {
            console.log('Attempting Load Sensor At: ', filePath);
            var device = require(filePath);
        }
        catch (err) {
            console.error("Failed to Load Sensor at: %s \n At: %s", filePath, err.stack);
        }
        if (typeof device !== 'undefined') {
            device.emitter = this.emitter;
        }
        if (device instanceof _1.Sensor) {
            Object.defineProperty(this, device.name, {
                value: device
            });
            //device.enable();
            this.sensors.push(device);
            return device;
        }
        else if (device instanceof _1.Controller) {
            Object.defineProperty(this, device.name, {
                value: device
            });
            this.controllers.push(device);
            //device.enable();
            return device;
        }
    }
    /** Get Loaded Device from Device name or File Name
     * @method
     * @param {string} deviceName - The Device FileName of Device Name
     * @returns {Sensor | Controller} - Device of Type Either Sensor or Controller
     */
    getDevice(deviceName) {
        //TODO Check For Device Type
        let device = Object.getOwnPropertyDescriptor(this, deviceName);
        return device.value;
    }
    /** Enables all loaded Sensors and Controller
     * @method
     */
    enable() {
        if (!this.enabled) {
            let sensors = this.sensors;
            let controllers = this.controllers;
            if (typeof sensors !== 'undefined') {
                sensors.forEach(sensor => {
                    sensor.enable();
                });
            }
            else {
            }
            if (typeof controllers !== 'undefined') {
                controllers.forEach(controller => {
                    controller.enable();
                });
            }
            this.enabled = true;
            this.emitter.emit('garden.enable', (this));
        }
        else {
        }
    }
    /** Disables all Loaded Controllers and Sensors
     * @method
     */
    disable() {
        if (this.enabled) {
            let sensors = this.sensors;
            let controllers = this.controllers;
            if (typeof sensors !== 'undefined') {
                sensors.forEach(sensor => {
                    sensor.disable();
                });
            }
            else {
            }
            if (typeof controllers !== 'undefined') {
                controllers.forEach(controller => {
                    controller.disable();
                });
            }
            this.enabled = false;
            this.emitter.emit('garden.disable', (this));
        }
        else {
        }
    }
    export() {
        var now = Date.now();
        var sensors = this.sensors.map(s => s.export());
        var Export = {
            Name: this.name,
            Enabled: this.enabled,
            Sensors: sensors
        };
        return Export;
    }
}
exports.Garden = Garden;
//# sourceMappingURL=Garden.js.map