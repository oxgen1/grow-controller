declare const EventEmitter: any;
import { ConnectionManager } from '.';
/**
 * @memberof Garden
 * @class GardenEvent
 * @extends {EventEmitter}
 */
export declare class GardenEvent extends EventEmitter {
    private connectionManager;
    constructor();
    get getConnectionManager(): ConnectionManager;
    set setConnectionManager(connectionManager: ConnectionManager);
}
export {};
//# sourceMappingURL=GardenEvent.d.ts.map