/* eslint-disable no-undef */
const assert = require('chai').assert;
const path = require('path');
/*
before('Set Globals and Load Library', function (done) {
    //Lets Define out Globals That would normally be defined in Server.js but we're not testing Server.js here were testing Garden.js
    global.__gardendir = path.join(__dirname, 'TestGarden');
    global.__gardenfile = path.join(__dirname, 'TestGarden', 'GardenTest.js');
    //Require Garden
    done();
});
**/


describe("Garden Basic Tests", function () {
    beforeEach('Load Garden and Create New Garden and Garden event', function (done) {
        const {
            Garden,
            GardenEvent
        } = require('../build');

        this.gardenEvent = new GardenEvent();
        this.garden = new Garden('Test Garden', 'Test Desc', this.gardenEvent);
        done();
    });

    it("Garden Should Load Garden Library", function (done) {
        var {Garden} = require('../build');
        assert.instanceOf(this.garden, Garden);
        done();
    });

    it('Garden Must Enable with out errors', function (done) {
        this.garden.enable();
        assert.doesNotThrow(this.garden.enable, 'Enable Does not Throw Errors');
        assert.isTrue(this.garden.enabled, 'Garden Enabled is True');
        done();
    });

    it('Garden Must Disable with out errors', function (done) {
        this.garden.disable();
        assert.doesNotThrow(this.garden.enable, 'Enable Does not Throw Errors');
        assert.isFalse(this.garden.enabled, 'Garden Enabled is False');
        done();
    });

    it('Garden Must Be able to load a Sensor', function(done){
        let sensor = this.garden.attach(path.join(__dirname, '/TestGarden/Sensors/SensorTest'));
        assert.doesNotThrow(this.garden.attach, 'Load Sensor Does not Throw Errors');
        const { Sensor } = require('../build');
        assert.instanceOf(sensor, Sensor, 'Load Sensor Must Return A Sensor');
        this.garden.enable();
        assert.isTrue(sensor.enabled, 'Loaded Sensor is Enabled');
        done();
    });
});
