const {Garden, GardenEvent, Server} = require('../../build');
const TriggerManager = require('../../build/Trigger/TriggerManager');
const Condition = require('../../build/Trigger/Condition');
var gardenEvent = new GardenEvent();
var testGarden = new Garden('testGarden', 'Test Desc', gardenEvent);
//var sensor1 = testGarden.loadSensor('SensorTest');

var sensor1 = testGarden.attach(__dirname + '/Sensors/TestSensor1');
var sensor = testGarden.attach(__dirname + '/Sensors/SensorTest');

var controller1 = testGarden.attach(__dirname + '/Controllers/ControllerTest');

var condition = Condition.createCondition(10, '<', sensor1, 'Temperature');
//condition.ref(sensor1, 'Temperature');

var condition2 = Condition.createCondition(30, '>', sensor1, 'Humidity');
//condition2.ref(sensor1, 'Humidity');

var condition3 = Condition.createCondition(210, '>', sensor1, 'Temperature');
//condition3.ref(sensor1, 'Temperature');

var interval = Condition.createInterval(10);
//var condition4 = Condition.createCondition(100, '>');
//condition4.ref(sensor1, 'Humidity');

var triggerManager = new TriggerManager(testGarden);
//var trigger = triggerManager.createTrigger(condition, 'test.on');
//console.log(trigger.or(condition2).or(condition3).init());

//var trigger2 = triggerManager.createTrigger(interval, 'test.on').and(condition2).init();
var trigger2 = triggerManager.createTrigger(interval, 'test.on').init();

console.log("Trigger Test with Interval: ", trigger2);

console.log(interval.id);

setTimeout(() => {
    //console.log("Export: ", testGarden.export());
}, 2000);

const server = new Server(testGarden);

server.connect('My Lit Garden');

server.start();