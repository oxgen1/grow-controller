const { Sensor } = require("../../../build");


var test = new Sensor({
    name: 'Test Sensor 1',
    dataFields: {Temperature: {type: 'FLOAT'}, Humidity: {type: 'FLOAT'}},
    sampleRate: 2000,
    verbose: true
});


//console.log(test.model);

test.sample = async function () {
    let tableNames = Object.keys(test.dataFields);
    let temp = Math.random() * 100;

    return {Temperature: temp, Humidity: temp, Time: 12312312};
}

//test.dataStream.on('data', (chunk) => { console.log(chunk); });

test.getLatestData(100);
//console.log(test);

module.exports = test;