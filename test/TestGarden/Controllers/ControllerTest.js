const { Controller, ControllerTimer } = require("../../../build");


const actionOn = () => {
    setTimeout(() => console.log('==================Action On Done==================\n', this), 10000);
};

const actionOff = () => {
    console.log('==================Action Off Done==================\n', this);
};


var test = new Controller({id:'test', name:'test'});

test.registerActionEvent('on', actionOn);
test.registerActionEvent('off', actionOff);

module.exports = test;
