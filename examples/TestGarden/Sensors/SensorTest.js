const { sql, Sensor } = require("../../../src");
const Sequelize = require('sequelize');


var test = new Sensor({
    id: 2,
    name: 'Test Sensor Zero',
    dataFields: {Temperature: {type: Sequelize.FLOAT}, Humidity: {type: Sequelize.FLOAT}},
    sampleRate: 2000,
    verbose: true
});


//console.log(test.model);

test.sample = async function () {
    let tableNames = Object.keys(test.dataFields);
    let temp = Math.random() * 100;

    return {Temperature: temp, Humidity: temp, Time: 12312312};
}

//test.dataStream.on('data', (chunk) => { console.log(chunk); });

test.getLatestData(100);
//console.log(test);

module.exports = test;
