# **Grow Controller**

# Overview
This is an attempt at a platform / library that will allow the generation of a web based dashboard. This dashboard will ideally chart live physical sensor data. As well it will allow for Conditional Triggering based on current sensor data and intervals. 

## Compatibility:
* Raspberry Pi 4
* Raspberry Pi Zero W

## Requirements: 
* Node Js Version: 12 or greater (I think)
* See Package.json


## Running

1. Build Src with typescript ```npm run build```
2. Run Test Garden Garden File with node ```node test/TestGarden/GardenTest.js```

## Examples:

### Current Garden Implementation
```javascript
    const { Garden, GardenEvent } = require('../build');
    const path = require('path');

    var gardenEvent = new GardenEvent(); // This needs to be depricated what kind there is zero reason why this has to be instanciated right here.
    var garden = new Garden('Jacks Garden', 'Test Desc', gardenEvent);
    garden.attach(path.join(__dirname, '/Sensors/SensorDHT22')); // Load the sensor

    const server = new Server(testGarden); // This Defines how the Program will interact with my API

    server.connect('My Lit Garden'); // This Defines a garden name in the future this should be an API key.

    server.start(); // Start everything
```
### Sensor Example:
```javascript
    const {Sensor } = require("../../build");
    const Sampler = require('./Samplers/DHT22Sampler');


    var DHT22 = new Sensor({
        id: 'TempHumiSensor',
        name: 'DHT22',
        dataFields: {Temperature: {type: 'FLOAT'}, Humidity: {type: 'FLOAT'}},
        deviceType: 'enviroment',
        sampleRate: 2000,
        verbose: false
    });


    DHT22.sample = async function () {
        //let tableNames = Object.keys(DHT22.dataFields);
        let envData = await Sampler.getEnvData();

        if (typeof envData !== 'undefined') { 
            return envData;
        } else {
            throw new Error('Sensor Returned no Data');
        }
    }

    //DHT22.getLatestData(100);
    //console.log(DHT22);

    module.exports = DHT22;
```
The Sampler is any async function that will return an object in the data fields outlined in the *dataFields Object*

#### Here is my Example Using a DHT22 Sensor:
```javascript
    var sensor = require('node-dht-sensor');
    var envData;
    async function getEnvData(){
        sensor.read(22, 4, function(err, temperature, humidity) {
            if (!err) {
               envData = {Temperature: temperature * 9 / 5 + 32, Humidity: humidity,    Time: Math.floor(Date.now() / 1000)};
            } else {
                envData = null;
                console.log('getEnvData: there was an error: ' + err);
                return err;
            }
        });
        console.log("DHT22Helper", envData);
        return envData;
    }

    module.exports = {
        getEnvData
    };
```