const { Garden, GardenEvent } = require('../build');
const path = require('path');

var gardenEvent = new GardenEvent();
var jacksGarden = new Garden('Jacks Garden', 'Test Desc', gardenEvent);
jacksGarden.attach(path.join(__dirname, '/Sensors/SensorDHT22'));



module.exports = jacksGarden;
