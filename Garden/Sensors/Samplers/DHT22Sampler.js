var sensor = require('node-dht-sensor');

var envData;
async function getEnvData(){
    sensor.read(22, 4, function(err, temperature, humidity) {
        if (!err) {
           envData = {Temperature: temperature * 9 / 5 + 32, Humidity: humidity, Time: Math.floor(Date.now() / 1000)};
        } else {
            envData = null;
            console.log('getEnvData: there was an error: ' + err);
            return err;
        }
    });
    console.log("DHT22Helper", envData);
    return envData;
}

module.exports = {
    getEnvData
};
