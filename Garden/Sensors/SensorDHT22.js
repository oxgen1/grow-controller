const {Sensor} = require("../../build");
const Sampler = require('./Samplers/DHT22Sampler');


var DHT22 = new Sensor({
    id: 1,
    name: 'DHT22',
    dataFields: {Temperature: {type: "FLOAT"}, Humidity: {type: "FLOAT"}},
    sampleRate: 2000,
    verbose: false
});


if (DHT22.verbose) console.log(DHT22.model);

DHT22.sample = async function () {
    //let tableNames = Object.keys(DHT22.dataFields);
    let envData = await Sampler.getEnvData();
    
    if (typeof envData !== 'undefined') { 
        return envData;
    } else {
        throw new Error('Sensor Returned no Data');
    }
}

//DHT22.getLatestData(100);
//console.log(DHT22);

module.exports = DHT22;
