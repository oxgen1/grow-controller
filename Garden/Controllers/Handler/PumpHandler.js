var rpio = require('rpio');

var pumpPin = 12;

function pumpCycle(wateringTime){
        var pumpOnData = pumpOn();
        if (!pumpOnData.success){
            return false;
        }
        setTimeout(() => {
           var pumpOffData = pumpOff();
           console.log(pumpOffData);
           if(!pumpOffData.success){
               return "Something Went wrong the Pump Might be on";
           }
        }, wateringTime / 1000);    
}


function pumpOn() {
    rpio.open(pumpPin, rpio.OUTPUT);
    if (rpio.read(pumpPin) == 0) {
        rpio.write(pumpPin, 1);
        console.log('pump on');
        return {
            startTime: Date.now(),
            success: true
        };
    } else {
        return {
            startTime: null,
            success: false,
            err: 'Pump is Already On'
        };
    }
}

function pumpOff() {
    rpio.open(pumpPin, rpio.OUTPUT);
    if (rpio.read(pumpPin) == 1) {
        rpio.write(pumpPin, 0);
        console.log('pump off');
        return {
            endTime: Date.now(),
            success: true
        };
    } else {
        return {
            startTime: null,
            success: false,
            err: 'Pump is Already Off'
        };
    }
}

module.exports = {
    pumpOn, pumpOff, pumpCycle
};
