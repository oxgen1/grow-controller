const boxen = require('boxen');
const {cyan, blue, red, bold, reset} = require('kleur');

const fields = ['Device Name', 'Status', 'Device Type', 'Test', 'Enabled', 'Sensor', 'Test', 'Enabled', 'Sensor', 'Test', 'Enabled', 'Sensor', 'Device Name', 'Status', 'Device Type', 'Test', 'Enabled', 'Sensor', 'Test', 'Enabled', 'Sensor', 'Test', 'Enabled', 'Sensor'];

const values = ['Test', 'Enabled', 'Sensor'];


console.log(boxen(bold().cyan(fillWhiteSpace(fields)), {padding: 1}));

console.log('Terminal size: ' + process.stdout.columns + 'x' + process.stdout.rows);

process.stdout.write(fillWhiteSpace(fields) + "\n");
//process.stdout.write(fillWhiteSpace(values));


function fillWhiteSpace(strings, marginRight=0){
    let output = "";
    let columns = process.stdout.columns;
    //let rows = process.stdout.rows;
    let columnStartPos = (columns/strings.length);

    strings.forEach((element, i) => {
        let numSpaces = columnStartPos;
        if(i == strings.length - 1){
            numSpaces =+ marginRight;
        }
        output += element.padEnd(numSpaces, ' ');
    });
    return output;
}
