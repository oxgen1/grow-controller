const garden = require('./GardenTest');
const ipc = require('node-ipc');

ipc.config.id = 'garden';
ipc.config.retry = 1500;


ipc.serve(() => { 
    console.log('Server Started');
});

ipc.server.on('app.start', (data, socket) => {
    ipc.log('Got a Start Message: ', data);
});

ipc.server.on('app.stop', (data, socket) => {
    
});

ipc.server.start();

