const {underline, red, green, bold} = require('kleur');
const boxen = require('boxen');

const stylizeFieldValues = (fields, values, options) => {
    
    const defaults = { capitalizeFirstLetter: true, colorizeBooleans: true, drawBorder: true};
    options = Object.assign({}, defaults, options);
    if(options.capitalizeFirstLetter) fields = fields.map(s => capitalizeFirstLetter(s));
    //console.log('Options: ', options);
    
    let fieldString = bold(underline(fillWhiteSpace(fields))); 
    let valuesStrings = [];
    if(values instanceof Array){
        values.forEach((value, index) => {
            let valueClean = value;
            value = value.map(s => { 
                if(options.colorizeBooleans && typeof s == 'boolean') return s ? green(s.toString()) : red(s.toString()) 
                else return s; 
            });
            valuesStrings.push(fillWhiteSpace(value, 0, valueClean, true));
            //console.log('Values String: ', valuesStrings);
        });
    }
    let out = options.drawBorder ? boxen(fieldString + "\n\n" + valuesStrings.join("\n")) : fieldString + "\n\n" + valuesStrings.join("\n");
    return out;
}

/**
 * Function for Ditributing Values evenly along the terminal size
 * @param {String[]} strings 
 * @param {int} paddingRight - Optional @default 0
 * @returns {String} - The String with proper whitespacing
 */
function fillWhiteSpace(strings, paddingRight=0, stringsClean, colorCodeComp = true){
    let output = "";
    let columns = process.stdout.columns;
    //let rows = process.stdout.rows;
    let columnStartPos = (columns/strings.length);

    strings.forEach((element, i) => {
        if(typeof element !== 'string'){
            element = element.toString();
        }
        let numSpaces = columnStartPos;
        if(i == strings.length - 1){
            numSpaces =+ paddingRight;
        }
        
        if(colorCodeComp && typeof stringsClean !== 'undefined'){
            let colorComp = element.length - stringsClean[i].toString().length;
            numSpaces += colorComp;
        }  
        output += element.padEnd(numSpaces, ' ');
    });
    return output;
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

module.exports = {fillWhiteSpace, stylizeFieldValues}
