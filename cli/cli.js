const commander = require('commander');
const asciichart = require('asciichart');
const { spawn, fork } = require('child_process');
const ipc = require('node-ipc');
const fs = require('fs');
const path = require('path');
const boxen = require('boxen');
const {cyan, blue, red, bold, reset} = require('kleur');
const myUtils = require('./utils');
//const garden = require('../tests/GardenTest');
//const GardenAdapterC = require('./GardenAdapter')
//var gardenAdapter = new GardenAdapterC(garden);
var processUserId = process.getuid();
const tempStorePath = `/tmp/garden-controller/${processUserId}`;

if(ensureDirectoryExistence(tempStorePath)){
    if(fs.existsSync(tempStorePath)){
        //console.log("file exisits");
    } else {
        console.log('File not Found Creating');
        fs.writeFileSync(tempStorePath, "");
    }
}


function ensureDirectoryExistence(filePath) {
  var dirname = path.dirname(filePath);
  if (fs.existsSync(dirname)) {
    return true;
  }
  ensureDirectoryExistence(dirname);
  fs.mkdirSync(dirname);
}


const program = new commander.Command();
program.version(0.1);

//Set Global Args
program.option('-v, --verbose', 'output extra debugging', false);

program.parse(process.argv);




const verbose = program.verbose;

const print = string => process.stdout.write(string + '\n')

//Setup IPC Connection.
ipc.config.maxRetries = 1;
ipc.config.stopRetrying = true;
ipc.config.silent = !verbose;

ipc.connectTo('testGarden');

//console.log(program.opts());

program.command('start').action(() => {
        ipc.of.testGarden.emit('app.start');
});

program.command('stop').action(() => {
    ipc.of.testGarden.emit('app.stop');
});

program.command('status').action(() => {
    ipc.of.testGarden.emit('app.getStatus');
});

program.command('read [sensor]').action((sensor) => {
    ipc.of.testGarden.emit('app.read', sensor);
});

program.command('chart').action(() => {
    var s = []
    for (var i = 0; i < 100; i++) s[i] = 15 * Math.cos(i * ((Math.PI * 8) / 120)) // values range from -15 to +15
    print(asciichart.plot(s, { height: 3 })) // this rescales the graph to ±3 lines
});

program.parse(process.argv);

/* Client Side Event Listeners */
ipc.of.testGarden.on('render', (data, socket) => {
    if (verbose) console.log('Raw Data:', data);

    let fields, values;
    fields = data.lists[0].fields;
    values = data.lists[0].values;
    print(myUtils.stylizeFieldValues(fields, values));
    process.exit(0);
});

ipc.of.testGarden.on('error', (err) => {
    switch(err.code){
        case 'ECONNREFUSED':
            print('Have You Started the Server?');
    }
    console.log(err.code);
    process.exit(1);
});

ipc.of.testGarden.on('done', (data, socket) => {
    console.log('Success');
    let error = data;

    process.exit(error);
});
