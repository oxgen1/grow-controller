/**
 * @file - Provides the Sensor Object
 */
import {Device} from "../";
import {Readable} from 'stream';


/**
 * @memberof Devices
 * @class Sensor
 * @extends Device
 * @constructor
 * @param {sensorData} sensorData - An Settings for the Sensor Object
 * @param {Object} sensorData.dataFields - This Uses Sequlize Data Types to Build the Table so this must be accurate, @example {FieldName1: {type: Sequelize.FLOAT}, FieldName2: {type: Sequelize.FLOAT}}
 * @param {int} sensorData.sampleRate - Sample Rate in ms
 * @example 
 * var test = new Sensor({
    id: 1,
    name: 'test',
    dataFields: {Temperature: {type: Sequelize.FLOAT}, Humidity: {type: Sequelize.FLOAT}},
    sampleRate: 2000,
    verbose: true
 * });
 * 
 */
interface SensorData{
    name: string;
    dataFields: TableColumn[];
    sampleRate: number;
    verbose?: boolean;
    deviceType: string;
}
export class Sensor extends Device {
    dataFields: Table;
    sampleRate: number;
    dataStream: Readable;
    intervalTimer: Timeout;
    constructor(sensorData : SensorData) {
        super(sensorData);
        this.dataFields = sensorData.dataFields;
        this.dataFields.Time = {type: 'INTEGER', allowNull: false};
        /**
         * @name Sensor#model
         * @type {int}
         */
        this.sampleRate = sensorData.sampleRate;

        //this.emitter = emitter;
        /**Defines a Sequlize Database Model
         * @name Sensor#model 
         * @type {SequlizeModel}
        */
        //this.model = sql.define(this.name +"-" +this.id, this.dataFields);
        /**Define a Readable Stream for Data
         * @type {ReadableStream}
         */
        this.dataStream = new Readable({
            objectMode: true,
            read() {}
        });
    }

    /**
     * Reads Sensor Data
     *
     * @returns {Object}
     */
    read(){
        if(!this.enabled){
            return false;
        }
        return this.dataStream.read();
        
    }
    /** Gets the Status of this Sensor.
     * @method
     * @returns {SensorStatus} 
     */
    getStatus(){
        return {
            name: this.name,
            enabled: this.enabled,
            type: this.constructor.name
        };
    }
    
    export() {
        return Object.assign(this.getStatus(), {data: this.read() }, {timestamp: Date.now()});
    }

    /** Enable or Start the Sensor, Will stop Polling for data
     * @method
     */
    enable(){
        if(!this.enabled){
            try {
                this._init();
                this.gardenEmitter.emit('sensorEnabled', (this));
                this.enabled = true;

            } catch (err){
                throw new Error(err)
            } finally{
            }
        } else {
            return this;
        }
    }
    /** Disable to Stop the Sensor, Will stop Polling for data
     * @method
     */
    disable(){
        if(this.enabled){
            try {
                clearInterval(this.intervalTimer);
                this.gardenEmitter.emit('sensorDisabled', (this));
            } catch (err){

            } finally {
                this.enabled = false;
            }
        }
    }
    
    /**
     * Gets the Most Recent Number of Fields from the database
     * @method
     * @param {int} numOfPoints 
     * 
     * @returns {Array} LatestData
     */
    getLatestData(numOfPoints){
        //this.model.findAll({limit: numOfPoints, order: ['Time', 'DESC']}).then((result) => {
        //    if (this.verbose) log.debug("getLatestData Called: ", result)('sensor');
        //});
    }
    /**
     * Gets Sensor Data inbetween 2 time periods, with an optional sorting parameter.  
     * 
     * @param {*} startTime 
     * @param {*} endTime 
     * @param {*} sorting 
     */
    getData(startTime, endTime, sorting='DESC'){

    }

    /**
     * @async
     * @abstract
     */
    async sample() {

    }
    /**
     * @private 
     * @async
     * @method
     */
    async _logData(){
        try {
            let data = await this.sample();
        
            //Are all the Field names and datatypes okay?
            if (!this._validateData(data)) {
                console.error('Invalid Data/DataFields');
            }
            /**
             * @event sensor.data
             */
            this.gardenEmitter.emit('sensor.data', this.id, data);
            this.dataStream.push(data);
            
        } catch (err) { 
            throw new Error(err.message);
        }
    }
    /**
     * Initialize Interval Timer, Should not be called outside of this class.
     * @private 
     * @method
     */
    private _init() {
        //this.model.sync();
        
        var _this = this;
        this.intervalTimer = setInterval(function () {
            _this._logData();
        }, this.sampleRate);
    }
    /**
     * @private
     * @param {*} dataObject 
     */
    private _validateData(dataObject){
        let tableNames = Object.keys(this.dataFields);

        let dataNames = Object.keys(dataObject);
        //Preform a check in ensure that the data fields the sensor is returning and return the values it is expecting.
        var diff = dataNames.filter(function(i) {
                return tableNames.indexOf(i) < 0;
        });
        //If we find any Throw an Error, for now we will not handle this but in the future we might not to cause a crash just because one sensor is out of alightment.
        if(diff.length > 0){
           throw new Error('You are returning an incorrect Data Field the expected Field Name should be: ' + diff);
        }
        return true;
    }
    /**
     * @private 
     * @method
     * @returns {Object} - Database Status Object
     */
    private _getDatabaseStatus(){

    }
}

/*class SensorEvent extends EventEmitter {
    constructor(){
        super();
    }
} */
