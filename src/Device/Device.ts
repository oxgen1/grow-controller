/**
 * @file
 * @namespace Devices
 */

const { Readable } = require('stream');
import {GardenEvent} from '../' 
//const EventEmitter = require('events');


/** An Abstarct Base Class to Represent Devices
 * @memberof Devices
 * @class Device
 * @abstract
 * @constructor
 * @param {Object} obj - Device Settings Object
 * @param {string} obj.id - Device unique name
 * @param {string} obj.deviceType - Device Type
 * @param {boolean} obj.verbose - Enable/ Disabnle Verbose Debugging
*/
export abstract class Device {
    id: string;
    name: string;
    deviceType: string;
    enabled: boolean;
    verbose: boolean;
    logOut: ReadableStream;
    gardenEmitter: GardenEvent;
    constructor(obj: {name: string; deviceType: string; verbose?: boolean; }) {
        this.id = obj.name.replace(" ", "-").toLowerCase();
        this.name = obj.name;
        this.deviceType = obj.deviceType;

        /**
         * @name Device#enabled 
         * @type {boolean}
         * @default false
        */
        this.enabled = false;
        /**
         * @name Device#verbose
         * @type {boolean}
         * @default false
         */
        this.verbose = (obj.verbose !== undefined) ? obj.verbose : false;
        this.logOut = new Readable({
            objectMode: false,
            read() {}
        });
    }

    
    /** 
     * Sets the Event Emitter for this Device. (Will Come from the Garden Object)
     * @type {Garden.GardenEvent}
     */
    set emitter(emitter: GardenEvent){
        this.gardenEmitter = emitter;
    }
    /**
     * Gets the Event Emitter for this Device. (Will Come from the Garden Object)
     */
    get emitter() : GardenEvent{
        return this.gardenEmitter;
    }
    /** Enable Device
     * @abstract
     * @method
     */
    abstract enable(): void;
    /** Disable Device
     * @abstract
     * @method
     */
    abstract disable() : void;
    /**
     * @abstract
     * @method
     */
    abstract getStatus() : void;
}