interface SensorExport {
    name: string;
    enabled: boolean;
    type: string;
}