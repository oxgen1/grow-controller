const path = require('path');

export = {

   getParentDirectory(inModule) {
        let parentOrModule = inModule;
        while (parentOrModule.hasOwnProperty('parent') && typeof parentOrModule.parent === 'object') {
            if (!parentOrModule['parent']) {
                return path.dirname(parentOrModule.filename);
            }
            parentOrModule = parentOrModule.parent;
        }
    }

}