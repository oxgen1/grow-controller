import {WsHelper} from './WsHelper';
import { GardenEvent } from '../GardenEvent';
import { APIHelper } from './APIHelper';

interface ConnectionManagerOptions{
    
}
export class ConnectionManager {
    UseWebSocket: boolean;
    UseAPI: boolean;
    ws: WsHelper;
    api: APIHelper;
    constructor(ConnectionOptions : ConnectionManagerOptions) {

        this.UseWebSocket = true;
        this.UseAPI = true;
    }

    configureAPI(baseUrl : string, options){
        console.log("Configuring API for: " + baseUrl);
        this.api = new APIHelper(baseUrl);
        return this.api;
    }

    connectToWebSocket(url : string, options){
        console.log("Connecting to WebSocket At: " + url);

        let wsConnection = new WsHelper(url, options);
        this.ws = wsConnection;
        return wsConnection;
    }

    /**
     * sendMessage
     */
    public sendMessage(data: any) {
        this.ws.send(data);
    }
}