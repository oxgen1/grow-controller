import fetch from 'node-fetch';

export class APIHelper {
    static baseUrl: string;

    constructor(baseUrl : string){
        if(this.doesEndWithBSlash(baseUrl)){
            APIHelper.baseUrl = baseUrl;
        } else {
            APIHelper.baseUrl = baseUrl + '/';
        }
    }

    /**
     * Call node-fetch.Fetch with POST paraemters and JSON 
     * @param namespace 
     * @param endpoint 
     * @param callback
     */
    public async post(namespace: string, endpoint: string, data: string | object, callback: Function) {
        let URL = `${APIHelper.baseUrl}${namespace}/${endpoint}`;
        if (typeof data === 'string'){
            // Do nothing
        } else if (typeof data === 'object') {
            data = JSON.stringify(data);
        }
        let options = {
            method: 'post',
            body:    data,
            headers: { 'Content-Type': 'application/json' },
        }
        console.log("URL::::::::::::::::::::" , URL);
        fetch(URL, options)
            .then(res => res.json()
            .then(json => callback(json)));
    
    
    }

    private doesEndWithBSlash(string: string) : boolean { 
        return (string.indexOf('/', string.length - 1) === string.length - 1);
    }   
}