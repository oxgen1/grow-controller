import * as ws from 'ws';
import { ClientOptions } from 'ws';
import { promisify } from 'util';

export class WsHelper{
    static sendOptions = {};
    ws: ws;
    constructor(url: string, options : ClientOptions){
        
        this.ws = new ws(url, options);
        this.ws.on('open', function () {
            console.log('Connected to Websocket Server');
        });
        
        this.ws.on('error', function(err) {
             console.error("There was an error on the websocket: ", err);
            if(err.name === 'ECONNREFUSED'){
                console.log('Error connecting to Websocket: ', err);
            }
        });

       //this.ws.on('close', function (code, reason) {
       //    //TODO Remove this and make async
       //    console.log('WebSocket Closed because %s Exit Code %d', reason, code);
       //});
    }

    public send(data : any){    
        // const send = promisify(this.ws.send);
        this.ws.send(data, WsHelper.sendOptions);
    }
}