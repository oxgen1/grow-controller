/* eslint-disable no-extend-native */
import {IPC} from 'node-ipc';
import * as path from 'path';
import {
    Garden, 
    ConnectionManager
} from './';
import { EventEmitter } from 'events';

interface ServerAuth {
    gardenPublicKey?: string;
    gardenPrivateKey?: string;
}

interface ServerOptions {
    auth?: ServerAuth;
    enableIPC?: boolean;
    websocketIP?: string;
    apiIP?: string;
}
/**
 * This is the Start Point where the API is intilizied. 
 * 
 * Steps as I remeber (Check as I go)
 * 1. Parse Server Options
 * 2. Enable Connection Managers
 * 3. Enable Garden
 * 
 */
export class Server extends EventEmitter{
    garden: Garden;
    connectionManager: ConnectionManager;
    private options: ServerOptions;
    constructor(garden: Garden, options?: ServerOptions) {
        super();
        options = options ?? {};
        options.enableIPC = options.enableIPC ?? false;
        options.apiIP = options.apiIP ?? "http://localhost:6969";
        options.websocketIP = options.websocketIP ?? "http://localhost:7171";

        
        this.options = options;

        this.connectionManager = new ConnectionManager({});

        console.log(this.options);
        this.garden = garden;

        this.garden.emitter.setConnectionManager = this.connectionManager;
        /**
         * @event init
         */
        this.on('init', () => {
            if(this.options.enableIPC) {
                console.log('Enabling IPC')
                const ipc = new IPC;
                /* Initialize Inter Process Communiciation */
                ipc.config.id = garden.name;
                ipc.config.retry = 1500;
        
                ipc.serve(() => {
                    console.log('Starting IPC Server');
                });
                this._registerIPCEvents(ipc);
            }
        })

       this.on('start', () => {
        /* IPC Event Listeners Defined Here */
        this.garden.enable();
       });
    }

    public connect(gardenId : String){
        console.log("Connecting to External Server With Garden ID: %s", gardenId);
        this.connectionManager.configureAPI(this.options.apiIP, {});
        
        this.connectionManager.connectToWebSocket(this.options.websocketIP, {});

        this.garden.emitter.on('garden.enable', (garden : Garden) => {
            var data = garden.export();
            data["id"] = gardenId; 
            this.connectionManager.api.post("garden", "handshake", data, (json) => {
                console.log("Call back function test: ", json);
            });
        });

        this.garden.emitter.on('sensor.data', (sensorId : string, data : Object) => {
            //this.connectionManager.sendMessage({[sensorId]: data}); 

        });
    }

    public start(){
        this.emit('start');
    }

    private _registerIPCEvents(ipc: IPC){
         //Start Command Listener
         ipc.server.on('app.start', (data, socket) => {
            //TODO Confirm These Things.
            this.garden.enable();
            ipc.server.emit(socket, 'done', 0);
            ipc.log('Garden Started: ', data);
        });

        //Stop Command Listener
        ipc.server.on('app.stop', (data, socket) => {
            this.garden.disable();
            //ipc.log('Socket: ', socket);

            ipc.server.emit(socket, 'done', 0);

            ipc.log('Garden Stopped: ', data);
        });

        ipc.server.on('app.read', (data, socket) => {
            let sensor = this.garden.getDevice(data)
            //ipc.log(garden.getDevice(data));
            ipc.log(sensor.read());
        });

        //Status Command Listener
        ipc.server.on('app.getStatus', (data, socket) => {
            let statusTemplate = {};

            if (typeof data == 'string') {
                let sensor = Object.getOwnPropertyDescriptor(this.garden, data).value;
                ipc.server.emit(socket, 'render', sensor.getGardenStatus());
            } else {
                let gardenStatus = this.garden.getGardenStatus();
                /*statusTemplate.title = gardenStatus.garden;
                statusTemplate.fields = Object.keys(gardenStatus.sensors[0]);
                statusTemplate.values = [];
                gardenStatus.sensors.forEach(row => {
                    statusTemplate.values.push(Object.values(row));
                });
                statusTemplate.values.push() */

                let statusOut = this.formatter(gardenStatus.garden, gardenStatus.sensors, gardenStatus.controllers);
                ipc.log(statusOut);

                ipc.server.emit(socket, 'render', statusOut);
            }
        });

        ipc.server.start();
    }
    private formatter = (title, ...objects) => {
        let formattedOut = {
            title: title,
            lists: []
        };
        let format = {
            values: []
        };
        let keys = [];
        let counter = 0;
        if (objects instanceof Array) {
            format.fields = (Object.keys(objects[0][0]));
            keys[counter] = Object.keys(objects[0][0]);
            objects.forEach((object, n) => {
                if (object instanceof Array) {
                    //format.fields = (Object.keys(object[0]));
                    var seenKeys = false;
                    var stickyCounter = undefined;
                    object.forEach((obj, i) => {
                        if (!format.fields.equals(Object.keys(obj))) {
                            //If we see a difference in the field lists stop this format List.
                            //might need to swap this to the end of the function Or not
                            formattedOut.lists[counter] = format;
                            format = {
                                values: []
                            };
                            format.fields = (Object.keys(obj));

                            let foundKeyPos = format.fields.findInArray(keys);
                            if (foundKeyPos !== false) {
                                seenKeys = true;
                                stickyCounter = foundKeyPos;
                                formattedOut.lists[stickyCounter].values.push(Object.values(obj));
                                //console.log("Found Key Pos: ", foundKeyPos);
                            } else {
                                format.values.push(Object.values(obj));
                            }
                            keys[counter + 1] = Object.keys(object[0]);
                            counter++;
                        } else {
                            if (seenKeys) {
                                formattedOut.lists[stickyCounter].values.push(Object.values(obj));
                            } else {
                                format.values.push(Object.values(obj));
                            }
                        }
                    });
                    if (!seenKeys) formattedOut.lists[counter] = format;
                } else {
                    //TODO HANDLE NO ARRAY CASES
                }
            });
        } else {

        }
        return formattedOut;
    }
}
    

    //#region 
    // Warn if overriding existing method
    if (Array.prototype.equals)
        console.warn("Overriding existing Array.prototype.equals. Possible causes: New API defines the method, there's a framework conflict or you've got double inclusions in your code.");
    // attach the .equals method to Array's prototype to call it on any array
    Array.prototype.equals = function (array) {
        // if the other array is a falsy value, return
        if (!array)
            return false;

        // compare lengths - can save a lot of time 
        if (this.length != array.length)
            return false;

        for (var i = 0, l = this.length; i < l; i++) {
            // Check if we have nested arrays
            if (this[i] instanceof Array && array[i] instanceof Array) {
                // recurse into the nested arrays
                if (!this[i].equals(array[i]))
                    return false;
            } else if (this[i] != array[i]) {
                // Warning - two different object instances will never be equal: {x:20} != {x:20}
                return false;
            }
        }
        return true;
    }
    // Hide method from for-in loops
    Object.defineProperty(Array.prototype, "equals", {
        enumerable: false
    });

    Array.prototype.findInArray = function (arrays) {
        var index, array;

        for (index = 0; index < arrays.length; index++) {
            if (this.equals(arrays[index])) {
                return index;
            }
        }
        return false;
    }

    Object.defineProperty(Array.prototype, "findInArray", {
        enumerable: false
    });
    //#endregion