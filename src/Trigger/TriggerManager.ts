import { Garden, GardenEvent } from "..";

/**
 * @file
 * @namespace Triggers
 */
const createCondition = require('./Condition');
const EventEmitter = require('events');
/**
 * The Trigger Manager Class 
 */
export class TriggerManager {
  gardenEmitter: GardenEvent;
  emitter: any;
  triggers: any[];
  debug: boolean;
  /**
   * Creates an instance of TriggerManager.
   * @constructs TriggerManager
   * @param {Garden} garden {@link Garden}
   * @memberof Triggers
   */
  constructor(garden: Garden) {
    garden.triggerManager = this;
    /** Garden Wide Event Emitter
     * @type {Garden.GardenEvent} */
    this.gardenEmitter = garden.emitter;
    /** TriggerManager Wide Event Emitter
     * @type {external:EventEmitter}
     */
    this.emitter = new EventEmitter();
    this.triggers = [];
    this.debug = true;
  }


  /**
   * Creates A Trigger Object
   * @constructs Trigger
   * @param {Condition} condition {@link Condition} 
   * @param {String} actionEvent - Event Listener Name connected with the Controller Action Will. Emit GardenEvent Emitter
   * @returns {Triggers#Trigger}
   * @memberof Triggers
   */
  createTrigger(condition, actionEvent) {
    var registerListener = (trigger) => {
      //TODO Make Unique
      let internalEvent = trigger.internalEvent;
      let actionEvent = trigger.actionEvent;

      /**
       * This Defines Our Trigger Level Listener and will have all the logic to decide if we send the Garden Level Event or not
       * @listens Triggers#ConditionInternal
       */
      trigger.emitter.on(internalEvent, (condition) => {
        //this.log.debug(`[${condition.getType()}] Condition Triggered Internal Event Emitteted`)('condition');
        if (typeof trigger.triggerLimiter !== "undefined") {
          if (trigger.triggerLimiter.id !== condition.id) return;
        }
        var result = trigger.evaluateOtherConditions(condition);
        //this.log.debug('Internal Condition Event Fired Result: %s', result)('trigger');
        if (result) {
          this.gardenEmitter.emit(trigger.actionEvent);
          //this.log.info("Firing Action Event %s", actionEvent)('Trigger');
        }
      });
    }
    /**
     * Loads the Condition Setting specific Properties Defined in the Trigger Object
     * @function
     * @private
     * @param {Condition} condition {@link Condition}
     */
    var load = (condition) => {
      //Lets add an Id
      //Define Triggers Internal Event at the Condition Level
      condition.event = trigger.internalEvent;

      //If this is a ThresholdCondition
      if (condition.getType() === 'ThresholdCondition') {
        var _condition = {};
        _condition = Object.assign(_condition, condition);

        //If its a sensor Linked Conditional Trigger Condition
        if (condition.hasOwnProperty('sensor')) {
          condition.read = () => {
            let data = condition.lastData;
            return data[condition.dataField];
          }
          condition.evaluateState = _condition.function;
          condition.function = (value) => {
            //this.log.debug('Condition Expression: ' + value + ' ' + condition.operator + ' ' + condition.threshold)('condition');
            //this.log.debug('Condition Value: ', value)('condition');
            let state = _condition.function(value);
            //this.log.debug('Condition State: ', state)('condition');
            condition.lastTriggered = Date.now();
            condition.lastState = state;
            if (state) {
              try {
            //    this.log.debug('Value Truethy: ', state)('condition');
                condition.lastEmitted = Date.now();
                /**
                 * @fires Triggers#ConditionInternal
                 */
                this.emitter.emit(condition.event, condition, trigger);
              } catch (err) {
                console.error('Error While Trying to Emit Internal Condition Event: ', err);
              }
            }
            return state;
          }
        }
        //If this condition is Interval Type
      } else if (condition.getType() === 'IntervalCondition') {
        /**
         * @fires Triggers#ConditionInternal
         */
        condition.function = () => {
          condition.intervalObj = setInterval(() => {
            /** Internal Condition Event Emitter
             * @event Triggers#ConditionInternal 
             * @type {Conditions.Condition}
             * @type {Trigger}
             * 
             */
            this.emitter.emit(condition.event, condition, trigger);
          }, condition.interval);
        }
      }
    }

    /**
     * @name Triggers#Trigger
     * @property {int} id - The Id of the Trigger
     * @property {EventEmitter} emitter {@link EventEmitter}
     * @property {String} actionEvent - The event that this Trigger will trigger when it meets the conditions set
     * @property {String} internalEvent - An Internal Event that triggers before the actionEvent when one of the conditions are met
     * @property {Array.<Condition>|Array.linkingObjects} logic - An Array of Conditions and Linking Objects 
     * @property {Condition} triggerLimiter @default undefined - If set the we will only Trigger off of this one Condition, but check the other exsiting conditions before executing the action.
     */
    let trigger = {
      id: this.triggers.length,
      emitter: this.emitter,
      actionEvent: actionEvent,
      internalEvent: actionEvent + '-' + this.triggers.length,
      logic: [],
      triggerLimiter: undefined, // If this is set then we will only trigger off this one. Should be value of condition
      getInitCondition: function () {
        return this.logic[this.logic.map(e => e.type).indexOf('init') + 1];
      },
      getLogic: function () {
        return this.logic;
      },
      and: function (condition) {
        load(condition);
        this.logic.push({
          type: 'and'
        });
        this.logic.push(condition);
        return this;
      },
      or: function (condition) {
        load(condition);
        this.logic.push({
          type: 'or'
        });
        this.logic.push(condition);
        return this;
      },
      checkOtherConditions(condition) {
        let lastTriggered = condition.lastTriggered;
        let evaluatedArry = this.logic.reduce((acc, value, i, arr) => {
          if (value.hasOwnProperty("type")) { // These will be our linking Objects
            if (!arr[i + 1].hasOwnProperty("type") && !arr[i + 1].isEvaluable) return acc;
            if (value.type == 'init') {
              acc.push('init');
            } else if (value.type == 'and') {
              acc.push('&&');
            } else if (value.type == 'or') {
              acc.push('||');
            }
          } else if (value.isEvaluable) {
            if (value.lastTriggered > lastTriggered - 2000 && value.lastTriggered < +2000) {
              acc.push(value.lastState);
            } else {
              let v = value.read();
              acc.push(value.evaluateState(v));
            }
          } else {

          }
          return acc;
        }, []);
        return evaluatedArry;
      },
      evaluateOtherConditions: function (condition) {
        let evaluatedArr = this.checkOtherConditions(condition);
        if (evaluatedArr.length > 0) { // if we have no evaluteable conditions we should not evaluate
          var result = evaluatedArr.reduce((acc, curr, i, arr) => {
            if (i == 1) {
              acc = curr;
            }
            if (curr == '&&') {
              acc = acc && arr[i + 1];
            } else if (curr == '||') {
              acc = acc || arr[i + 1];
            }
            return acc;
          });
          //this.log.debug('Trigger Emitter evaluated: %s Array: ', evaluatedArr, result)('trigger-eval');
          return result;
        } else {
          return true;
        }
      },
      init: function () {
        let logic = this.getLogic();
        let type;
        logic.forEach((logicE, i) => {
          //if we have a type property we can assume it is a linking object
          if (logicE.hasOwnProperty('type')) {
            type = logicE.type;
          } else {
            if (logicE.getType() === 'ThresholdCondition') {
              if (logicE.hasOwnProperty('sensor')) {
                logicE.sensor.dataStream.on('data', (data) => {
                  //this.log.debug("data recived: ", data[logicE.dataField])('condition');
                  logicE.lastData = data;
                  logicE.function(data[logicE.dataField]);
                });
              }
            } else if (logicE.getType() === 'IntervalCondition') {
              logicE.function();
            }
          }
        });
        registerListener(this);

        return this;
      }
    }
    load(condition);
    if (condition.getType() === "IntervalCondition" && condition.isTriggerAble) {
      trigger.triggerLimiter = condition;
    }
    trigger.logic.push({
      type: 'init'
    });
    trigger.logic.push(condition);
    return trigger;
  }
}

module.exports = TriggerManager;