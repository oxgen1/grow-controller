import {Sensor} from './../';
/**
 * @memberof Conditions
 * @abstract
 */
class Condition {
    _isEvaluable: boolean;
    _isTriggerAble: boolean;
    _lockOut: boolean;
    _event: string;
    _lastTriggered: any;
    constructor() {
        this._isEvaluable = false;
        this._isTriggerAble = true;
        this._lockOut = false;
        this._event;
    }

    /**
     * Gets the A computed Id based on the Type, and other props
     * @readonly
     */
    get id(){
        let boolToChar = (bool: boolean) => bool ? 't' : 'f';
        return `${this.getType()}-${this._event}-[${boolToChar(this._isEvaluable)}${boolToChar(this._isTriggerAble)}${boolToChar(this._lockOut)}]`
    }

    /** Gets If the Condition is Evaluable */    
    get isEvaluable() {
        return this._isEvaluable;
    }
    /** Sets If the Condition is Evaluable
     * @type {boolean}
     */
    set isEvaluable(boolean) {
        this._isEvaluable = boolean;
    }
    /**Gets if the Condition is Triggerable  (i.e if this condition can by itself trigger an action*/
    get isTriggerAble() {
        return this._isTriggerAble;
    }
    /**Sets if the Condition is Triggerable  (i.e if this condition can by itself trigger an action
     * @type {boolean}
    */
    set isTriggerAble(boolean) {
        this._isTriggerAble = boolean;
    }
    /**Gets if the Condition is a lock out condition (I.e If this condition is not met no actions can be emitted) */
    get lockOut() {
        return this._lockOut;
    }
    /**Sets if the Condition is a lock out condition (I.e If this condition is not met no actions can be emitted) 
     * @type {boolean}
    */
    set lockOut(boolean) {
        this._lockOut = boolean;
    }
    /**Gets the Interval Trigger Level Event Name */
    get event() {
        return this._event;
    }
    /**Gets the Interval Trigger Level Event Name 
     * @type {string}
    */
    set event(string) {
        this._event = string;
    }
    /**Gets the Last Triggered Timestamp */
    get lastTriggered() {
        return this._lastTriggered;
    }
    /**Sets the Last Triggered Timestamp 
     * @type {timestamp}
    */
    set lastTriggered(timestamp) {
        this._lastTriggered = timestamp;
    }
    /**Gets The Type (Constructor.name) */
    getType() {
        return this.constructor.name;
    }
}
/**
 *
 * @class
 * @extends {Conditions.Condition}
 * @memberof Conditions
 * @abstract
 */
class ThresholdCondition extends Condition{
    operator: any;
    threshold: any;
    ref: string;
    sensor: Sensor;
    dataField: string;
    value: any;
    function: (value: any) => boolean;
    humanReadable(){
        return `X ${this.operator} ${this.threshold}`;
    }
}
/**
 * @class
 * @extends {Conditions.Condition}
 * @memberof Conditions
 * @abstract
 */
class IntervalCondition extends Condition{
    _interval: number;
    ref: string;
    constructor() {
        super();
        this._interval = 0;
    }
    get interval(){
        return this._interval;
    }
    set interval(seconds){
        this._interval = (seconds * 1000);
    }

    humanReadable(){
        return `Every ${(this.interval/1000)} Seconds`;
    }
}

/**
 * Creates a Interval Trigger Condition 
 * @memberof Triggers.Conditions
 * @param {int} interval - Interval In Seconds
 * @param {boolean} [lockOut=false]
 * @returns {IntervalCondition} {@link Conditions.IntervalCondition}
 * @example
 * var interval = new Condition.createInterval(10);
 */
const createInterval = function (interval, lockOut = false) {
    var condition = new IntervalCondition();
    condition.isEvaluable = false;
    condition.interval = interval;
    condition.ref = 'interval';
    condition.lockOut = lockOut
    return condition;
}

/**
 *
 * Will Create a function that evalutes as follows {value} {operator} {threshold} (Ex: 10 > 11)
 * @param {int} threshold - The Threshold to check against 
 * @param {string} operator - Any Conditional Operator Except && or ||
 * @param {boolean} [lockOut=false] {@default false}
 * @returns {ThresholdCondition} {@link Conditions.ThresholdCondition}
 * @memberof Triggers
 */
const createCondition = function (threshold, operator, value, field = '', lockOut = false) {
    var condition = new ThresholdCondition();
    condition.isEvaluable = true;
    if (value instanceof Sensor) {
        condition.ref = 'sensor';
        condition.sensor = value;
        condition.dataField = field;
        //condition.hasRef = true;
    } else {
        condition.ref = 'fixed';
        condition.value = value;
    }
    condition.threshold = threshold;
    condition.operator = operator;
    condition.lockOut = lockOut
    condition.function = (value) => {
        return compare(value, operator, threshold);
    };
    return condition;
}

export = {
    createCondition,
    createInterval
};

function compare(post, operator, value) {
    switch (operator) {
        case '>':
            return post > value;
        case '<':
            return post < value;
        case '>=':
            return post >= value;
        case '<=':
            return post <= value;
        case '==':
            return post == value;
        case '!=':
            return post != value;
        case '===':
            return post === value;
        case '!==':
            return post !== value;
    }
}

function chain(first, second, operator) {
    switch (operator) {
        case 'and':
            return first && second;
        case 'or':
            return first || second;
        case '&&':
            return first && second;
        case '||':
            return first || second;
    }

}
//let condition = createConditional('sensor', {sensor: 'test', datafield: 'Temperature', threshold: 10, operator: '>'});
//let condition1 = createConditional('sensor', {sensor: 'test', datafield: 'Temperature', threshold: 10, operator: '<'});