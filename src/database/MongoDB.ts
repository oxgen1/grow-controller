import {MongoClient, Db} from 'mongodb';

const mongoUser = encodeURIComponent('jack');
const mongoPw = encodeURIComponent('SuperServer9!');
const mongoDbURL = `mongodb://${mongoUser}:${mongoPw}@localhost:27017`;
const MongoDBName = 'Gardens';

const mongoDB = new MongoClient(mongoDbURL);
var _db : Db;
class MongoDb {
    async connect() : Promise<Db> {

        try{
            await mongoDB.connect();
            _db = mongoDB.db(MongoDBName);
        } catch (err){
            console.error('There was an error connecting to MongoDB: ', err);
        }
        return _db;
    }
    async getDB(): Promise<Db> {
        return _db;
    }
}
var mongoDb =  new MongoDb();
export {mongoDb};