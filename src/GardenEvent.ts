const EventEmitter = require('events');
import fetch from 'node-fetch';
import { Garden, Sensor, ConnectionManager } from '.';
import * as WebSocket from 'ws';
/**
 * @memberof Garden
 * @class GardenEvent
 * @extends {EventEmitter}
 */
export class GardenEvent extends EventEmitter{
    private connectionManager: ConnectionManager;
    constructor(){
        super();
        // THis can later moved in to a static function or something which can be called from server.connect(); 
        this.on('sensorEnable', (sensor: Sensor) => {
           console.log('Sensor Enabled: ' + sensor.name); 
        });

        this.on('sensorDisable', (sensor: Sensor) => {
            console.log('Sensor Disabled: ' + sensor.name); 
        });

        this.on('garden.enable', (garden : Garden) => {

            //this.getConnectionManager.connectToWebSocket("ws://localhost:7171/", {});

            console.log('Garden Enabled: ', garden.export());
            var data =  JSON.stringify(garden.export());
            console.log(data);
            /*fetch("http://localhost:6969/garden/handshake", {
                method: 'post',
                body:    data,
                headers: { 'Content-Type': 'application/json' },
            }).then(res => res.json())
            .then(json => console.log(json)); */
            
        });

        this.on('sensor.data', (sensorId, data) => {
            console.log("getting Sensor Data: ", sensorId, data);
        });

        this.on('error', (err) => {
            console.log(err);
        });
    }

    get getConnectionManager() : ConnectionManager {
        if(this.hasOwnProperty('connectionManager') && this.connectionManager instanceof ConnectionManager){
            return this.connectionManager;
        } else {
            console.log('Called Connection Manager before it was set');
            return new Proxy(new ConnectionManager(null), {
                apply: function(_target, _thisArg, _argumentsList) { 
                    return true;
                },
                get: function(_target, _prop, _receiver){
                    return () => {return false;};
                }
            });
        }
    }

    set setConnectionManager (connectionManager : ConnectionManager){
        this.connectionManager = connectionManager;
    }
}